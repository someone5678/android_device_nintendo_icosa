#
# Copyright (C) 2021 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

BOARD_VENDOR := nvidia
DEVICE_PATH := device/nintendo/icosa

# Partitions
BOARD_FLASH_BLOCK_SIZE             := 4096

BOARD_BOOTIMAGE_PARTITION_SIZE     := 26738688
BOARD_CACHEIMAGE_PARTITION_SIZE    := 268435456
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 26767360
BOARD_SYSTEMIMAGE_PARTITION_SIZE   := 2147483648
BOARD_USERDATAIMAGE_PARTITION_SIZE := 10099646976
BOARD_VENDORIMAGE_PARTITION_SIZE   := 805306368

$(foreach p, CACHE SYSTEM VENDOR, $(eval BOARD_$(p)IMAGE_FILE_SYSTEM_TYPE := ext4))
$(foreach p, SYSTEM VENDOR, $(eval TARGET_COPY_OUT_$(p) := $(call to-lower, $(p))))

BOARD_BUILD_SYSTEM_ROOT_IMAGE      := true

TARGET_USERIMAGES_USE_F2FS := true
TARGET_USERIMAGES_USE_EXT4 := true

# Releasetools
TARGET_RELEASETOOLS_EXTENSIONS := $(DEVICE_PATH)

# Manifest
DEVICE_MANIFEST_FILE := $(DEVICE_PATH)/configs/vintf/manifest.xml

# Bluetooth
BOARD_CUSTOM_BT_CONFIG := $(DEVICE_PATH)/bluetooth/include/vnd_icosa.txt
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := $(DEVICE_PATH)/bluetooth/include

# Kernel
ifeq ($(TARGET_PREBUILT_KERNEL),)
KERNEL_TOOLCHAIN        := $(shell pwd)/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-gnu-6.4.1/bin
KERNEL_TOOLCHAIN_PREFIX := aarch64-linux-gnu-
TARGET_KERNEL_SOURCE 	:= kernel/nvidia/linux-4.9_icosa/kernel/kernel-4.9
TARGET_KERNEL_CONFIG    := tegra_android_defconfig
BOARD_KERNEL_IMAGE_NAME := Image.gz
else
BOARD_VENDOR_KERNEL_MODULES += $(wildcard $(dir $(TARGET_PREBUILT_KERNEL))/*.ko)
endif

# Recovery
TARGET_RECOVERY_FSTAB        := $(DEVICE_PATH)/rootdir/etc/fstab.icosa_recovery
BOARD_SUPPRESS_EMMC_WIPE     := true
TARGET_RECOVERY_UPDATER_LIBS := librecoveryupdater_tegra
BOARD_USES_RECOVERY_AS_BOOT := false
TARGET_NO_RECOVERY := false
TARGET_RECOVERY_PIXEL_FORMAT := "BGRA_8888"
TARGET_USES_MKE2FS := true

# Security Patch Level
VENDOR_SECURITY_PATCH := 2021-04-05

# Treble
BOARD_PROPERTY_OVERRIDES_SPLIT_ENABLED := true
BOARD_VNDK_VERSION                     := current
PRODUCT_FULL_TREBLE_OVERRIDE           := true

# Assert
TARGET_OTA_ASSERT_DEVICE := icosa

# Bootloader versions
TARGET_BOARD_INFO_FILE := $(DEVICE_PATH)/board-info.txt

# IIO Sensor Defconfig
BOARD_SENSORS_STMICRO_IIO_DEFCONFIG := $(DEVICE_PATH)/configs/sensors/sensors_icosa_defconfig

# Init
TARGET_INIT_VENDOR_LIB := //$(DEVICE_PATH):libinit_icosa
TARGET_RECOVERY_DEVICE_MODULES := libinit_icosa

# TWRP Support
ifeq ($(WITH_TWRP),true)
include $(DEVICE_PATH)/twrp/twrp.mk
endif

include device/nvidia/t210-common/BoardConfigCommon.mk
